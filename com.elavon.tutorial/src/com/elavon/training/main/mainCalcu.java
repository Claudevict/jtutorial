package com.elavon.training.main;

import com.elavon.training.impl.implAwsmCalcu;

public class mainCalcu extends implAwsmCalcu{

	public static void main(String[] args) {
		implAwsmCalcu cal = new implAwsmCalcu();
		 
		System.out.println(cal.getQuotientAndRemainder(11,10));
		
		System.out.println(cal.getSum(5, 7));
		
		System.out.println(cal.getDifference(6, 4));
		
		System.out.println(cal.getProduct(9, 7));
		
		System.out.println(cal.toCelsius(152));
		
		System.out.println(cal.toFahrenheit(15));
		
		System.out.println(cal.toKilogram(11));
		
		System.out.println(cal.toPound(10));
		
		System.out.println(cal.isPalindrome("madam"));
		
		System.out.println(cal.isPalindrome("badang"));

	}

}
