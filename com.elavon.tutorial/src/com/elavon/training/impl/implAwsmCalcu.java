package com.elavon.training.impl;

import com.elavon.training.intrface.*;

public class implAwsmCalcu implements AwesomeCalculator {
	
		@Override
		public int getSum(int augend, int addend) {
			int getSum = augend + addend;
			return getSum;
		}

		@Override
		public double getDifference(double minuend, double subtrahend) {
			double getDifference = minuend - subtrahend;
			return getDifference;
		}

		@Override
		public double getProduct(double multiplicand, double multiplier) {
			double getProduct = multiplicand * multiplier;
			return getProduct;
		}

		@Override
		public String getQuotientAndRemainder(int dividend, int divisor) {
			int  x = dividend/divisor; 
			int y = dividend%divisor;
			return x + " remainder " + y ;
		}
		

		@Override
		public double toCelsius(int fahrenheit) {
			double toCelsius = 0.5555555555 * fahrenheit - 32;
			return toCelsius;
		}

		@Override
		public double toFahrenheit(int celsius) {
			double toFahreinheit = (9 / 5) * (celsius + 32);
			return toFahreinheit;
		}

		@Override
		public double toKilogram(double lbs) {
			double toKilogram = lbs / 2.2;
			return toKilogram;
		}

		@Override
		public double toPound(double kg) {
			double toPound = kg * 2.2;
			return toPound;
		}

		@Override
		public boolean isPalindrome(String str) {
			return str.equals(new StringBuilder(str).reverse().toString());
		}

}
