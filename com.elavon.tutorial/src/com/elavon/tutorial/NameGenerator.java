package com.elavon.tutorial;

public class NameGenerator {

	public static void main(String[] args) {
		System.out.println(" ******  **          ***    **     ** ********  ******** **     ** ****  ******  ********");
		System.out.println("**    ** **         ** **   **     ** **     ** **       **     **  **  **    **    **");
		System.out.println("**       **        **   **  **     ** **     ** **       **     **  **  **          **");
		System.out.println("**       **       **     ** **     ** **     ** ******   **     **  **  **          **");
		System.out.println("**       **       ********* **     ** **     ** **        **   **   **  **          **");
		System.out.println("**    ** **       **     ** **     ** **     ** **         ** **    **  **    **    **");
		System.out.println(" ******  ******** **     **  *******  ********  ********    ***    ****  ******     **");
		System.out.println(" ");
		System.out.println("******** ********     ***    **    **  ******  ****  ******");
		System.out.println("**       **     **   ** **   ***   ** **    **  **  **    **");
		System.out.println("**       **     **  **   **  ****  ** **        **  **");
		System.out.println("******   ********  **     ** ** ** ** **        **   ******");
		System.out.println("**       **   **   ********* **  **** **        **        **");
		System.out.println("**       **    **  **     ** **   *** **    **  **  **    **");
		System.out.println("**       **     ** **     ** **    **  ******  ****  ******");
		System.out.println(" ");
		System.out.println(" ******  **** **       **     **    ***");
		System.out.println("**    **  **  **       **     **   ** **");
		System.out.println("**        **  **       **     **  **   **");
		System.out.println(" ******   **  **       **     ** **     **");
		System.out.println("      **  **  **        **   **  *********");
		System.out.println("**    **  **  **         ** **   **     **");
		System.out.println(" ******  **** ********    ***    **     **");
		System.out.println(" ");
		System.out.println("********     ***    ********  **        *******");  
		System.out.println("**     **   ** **   **     ** **       **     **"); 
		System.out.println("**     **  **   **  **     ** **       **     **"); 
		System.out.println("********  **     ** ********  **       **     **"); 
		System.out.println("**        ********* **     ** **       **     **"); 
		System.out.println("**        **     ** **     ** **       **     **"); 
		System.out.println("**        **     ** ********  ********  *******"); 
	}

}
