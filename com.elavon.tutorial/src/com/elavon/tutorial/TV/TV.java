package com.elavon.tutorial.TV;

public class TV {
	
	protected String brand;
	protected String model;
	protected boolean power; 
	protected int channel;
	protected int volume;
	
	public TV(String brand, String model) {
		this.brand = brand;
		this.model = model;
		this.channel = 0;
		this.volume = 5;
	}
	
	public String toString() {
		return "Brand:" + this.brand + ",, " + " Model:" + this.model + ",, " 
				+ "Power:" + (power ? "on" : "off")  + ",, " + "Channel:" + this.channel + ",, " + "Volume:" + this.volume;
	}
	
	public void turnOn() {
		power = true;
	}
	
	public void turnOff() {
		power = false;
	}
	
	public void channelUp () {
		if(channel < 13) {
			channel++;
		}
		else channel = 1;
	}
	
	public void channelDown () {
		if(channel > 1) {
			channel--;
		}
		else channel = 13;
	}
	
	public void volumeUp () {
		if (volume < 10) {
			volume++;
		}
	}
	
	public void volumeDown () {
		if (volume > 0) {
			volume--;
		}
	}
	
	public static void main (String[] args) {
		
		TV tv = new TV("Andre Electronics", "ONE");
		System.out.println(tv);
		
		tv.turnOn();
		System.out.println(tv);
		
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelDown();
		tv.volumeDown();
		tv.volumeDown();
		tv.volumeDown();
		tv.volumeUp();
		tv.turnOff();
	
		System.out.println(tv);


	}
}
