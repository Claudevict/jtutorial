package com.elavon.tutorial.TV;

public class ColoredTV extends TV{

	int brightness;
	int contrast;
	int picture;
	
	public ColoredTV(String brand, String model) {
		super(brand, model);
		
		this.brightness = 50;
		this.contrast = 50;
		this.picture = 50;
		
	}
	
	public String toString() {
		return "Brand:" + this.brand + ",, " + " Model:" + this.model + ",, " 
				+ "Power:" + (power ? "on" : "off")  + ",, " + "Channel:" + this.channel + ",, " + "Volume:" + this.volume + ",,"
						+ " " + "b:" + this.brightness + ",, " + "c:" + this.contrast + ",," + "p:" + this.picture;
	}
	
	public void brightnessUp() {
			brightness++;
	}
	
	public void brightnessDown() {
			brightness--;
	}
	
	public void contrastUp() {
			contrast++;
	}
	
	public void contrastDown() {
			contrast--;
	}
	
	public void pictureUp() {
			picture++;
	}
	
	public void pictureDown() {
		picture--;
	}
	
	public int switchToChannel(int channel) {
		return channel;
	}
	
	public void mute() {
		this.volume = 0;
	}
	
	public static void main (String[] args) {
		TV bnwTV = new TV("Admiral", "A1");
		ColoredTV sonyTV = new ColoredTV("SONY", "S1");
		ColoredTV sharpTV = new ColoredTV("SHARP", "SH1");
		
		sonyTV.brightnessUp();
		sharpTV.mute();
		sharpTV.brightnessDown();
		sharpTV.brightnessDown();
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		sharpTV.pictureDown();
		
		
		System.out.println(bnwTV);
		System.out.println(sonyTV);
		System.out.println(sharpTV);
		
	}
}
