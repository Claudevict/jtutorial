package com.elavon.tutorial.xmas;

public class twelveDaysOfXmas {
	
	public static void printLyrics () {

		for (int c = 1; c <= 12; c++) {
			String day = "";
			switch (c) {
			case 1 : day = "First";
				break;
			case 2 : day = "Second";
				break;
			case 3 : day = "Third";
				break;
			case 4 : day = "Fourth";
				break;
			case 5 : day = "Fifth";
				break;
			case 6 : day = "Sixth";
				break;
			case 7 : day = "Seventh";
				break;
			case 8 : day = "Eight";
				break;
			case 9 : day = "Ninth";
				break;
			case 10 : day = "Tenth";
				break;
			case 11 : day = "Eleventh";
				break;
			case 12 : day = "Twelveth";
				break;
			}
			
			System.out.println("On the "+ day +" day of Christmas my true love sent to me,");
			
			switch (c) {
			
			case 12 : System.out.println("12 Drummers Drumming");
			case 11 : System.out.println("Eleven Pipers Piping");
			case 10 : System.out.println("Ten Lords a Leaping");
			case 9 : System.out.println("Nine Ladies Dancing");
			case 8 : System.out.println("Eight Maids a Milking");
			case 7 : System.out.println("Seven Swans a Swimming");
			case 6 : System.out.println("Six Geese a Laying");
			case 5 : System.out.println("Five Golden Rings");
			case 4 : System.out.println("Four Calling Birds");
			case 3 : System.out.println("Three French Hens");
			case 2 : System.out.println("Two Turtle Doves");
			case 1 : System.out.println(c == 1 ? "a Partridge in a Pear Tree" : "and a Partridge in a Pear Tree");
			}
			
			System.out.println();
		}

	}

	public static void main(String[] args) {
		twelveDaysOfXmas.printLyrics();
	}

}
